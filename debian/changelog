pycangjie (1.5.0.really.1.5.0-3) unstable; urgency=medium

  * Upload to unstable.

 -- Boyuan Yang <byang@debian.org>  Thu, 09 Jan 2025 10:44:44 -0500

pycangjie (1.5.0.really.1.5.0-2) experimental; urgency=medium

  * Rebuild against Python 3.13.
  * debian/control: Tighten version requirement of libcangjie-devv-tools
    and libcangjie-dev.

 -- Boyuan Yang <byang@debian.org>  Wed, 08 Jan 2025 12:56:59 -0500

pycangjie (1.5.0.really.1.5.0-1) experimental; urgency=medium

  * Team upload.
  * Upload to experimental with .really version string to allow
    software version reverting in Sid.

 -- Boyuan Yang <byang@debian.org>  Mon, 30 Sep 2024 23:49:48 -0400

pycangjie (1.5.0-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Boyuan Yang <byang@debian.org>  Sat, 14 Sep 2024 15:29:03 -0400

pycangjie (1.5.0-1~exp3) experimental; urgency=medium

  * Team upload.
  * debian/patches/0001-tests-meson.build-Extend-timeout-to-900s.patch: Edit
    meson config to extend the total timeout to 300s to avoid test timeout
    failure on slow architectures.

 -- Boyuan Yang <byang@debian.org>  Sat, 14 Sep 2024 13:04:46 -0400

pycangjie (1.5.0-1~exp1) experimental; urgency=medium

  * Team upload.
  * debian/control: Update package (build-)dependencies.
  * debian/rules: Also install the CHANGELOG.md file.
  * debian/copyright: Minor information update.

 -- Boyuan Yang <byang@debian.org>  Wed, 11 Sep 2024 14:13:06 -0400

pycangjie (1.3-3) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
    + debian/rules: Drop --fail-missing argument to dh_missing, which is now the
      default.
  * Set upstream metadata fields: Repository-Browse.

  [ Boyuan Yang ]
  * Update standards version to 4.7.0, no changes needed.
  * debian/control: Update homepage information.
  * debian/control: Add build-dep python3-setuptools. (Closes: #1080863)
  * debian/control: Update build-dep pkg-config => pkgconf.
  * debian/watch: Monitor new upstream location.

 -- Boyuan Yang <byang@debian.org>  Thu, 05 Sep 2024 17:41:10 -0400

pycangjie (1.3-2) unstable; urgency=medium

  * Team upload.
  * debian/control:
    + Bump Standards-Version to 4.5.0.
    + Bump debhelper compat to v12.
    + Use Build-Dep: dh-sequence-python3. (Closes: #949971)
  * debian/README.source: Dropped, useless.

 -- Boyuan Yang <byang@debian.org>  Wed, 05 Feb 2020 15:49:37 -0500

pycangjie (1.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release 1.3.
  * debian: Apply "wrap-and-sort -abst".
  * debian/control:
    + Bump Standards-Version to 4.2.1 (no changes needed).
    + Bump debhelper compat to v11.
    + Update maintainer email address and use Debian Input Method Team.
      (Closes: #899870)
    + Update Vcs-* fields and use git repo under Salsa input-method team.
  * debian/rules: Use "dh_missing --fail-missing".
  * debian/copyright: Use secure uri.
  * debian/patches:
    - Drop patches that are merged upstream.
    + Cherry-pick an upstream patch to fix typo.
  * debian/watch: Rewrite in v4 format and watch new upstream
    GitHub project.

 -- Boyuan Yang <byang@debian.org>  Mon, 05 Nov 2018 19:43:04 -0500

pycangjie (1.2-2) unstable; urgency=medium

  * Team upload.
  * Fix FTBFS.
    Thanks to Mathieu Bridon (Closes: #787312)

 -- ChangZhuo Chen (陳昌倬) <czchen@gmail.com>  Sun, 31 May 2015 21:26:51 +0800

pycangjie (1.2-1) unstable; urgency=medium

  * New upstream release.
  * Merge changes from last NMU, thanks Matthias for the fix.
    (Closes: #743284)

 -- Anthony Wong <yp@anthonywong.net>  Sun, 26 Oct 2014 16:46:43 +0800

pycangjie (1.1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix cython version check. Closes: #743112, #742690.
  * Drop build dependency on python3-all-dev, the package only builds
    for the default python3 version. Closes: #734339.

 -- Matthias Klose <doko@debian.org>  Tue, 01 Apr 2014 11:13:50 +0100

pycangjie (1.1-1) unstable; urgency=low

  * New upstream release.

 -- Anthony Wong <anthony.wong@ubuntu.com>  Sat, 15 Feb 2014 18:09:14 +0800

pycangjie (1.0-1) unstable; urgency=low

  * New upstream release.

 -- Anthony Wong <anthony.wong@ubuntu.com>  Sun, 05 Jan 2014 23:00:13 +0800

pycangjie (0.0.1-1) unstable; urgency=low

  * New upstream release 0.0.1
  * Upload to Debian (Closes: #712683)

 -- Anthony Wong <anthony.wong@ubuntu.com>  Sun, 15 Sep 2013 23:36:22 +0800

pycangjie (0.0.1~git20130315-0ubuntu1) raring; urgency=low

  * Initial release.

 -- Anthony Wong <anthony.wong@ubuntu.com>  Fri, 15 Mar 2013 05:25:29 +0800
